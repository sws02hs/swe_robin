Small changes to Robin Hogan's matlab shallow water model described at:
http://www.met.reading.ac.uk/~swrhgnrj/shallow_water_model/
Change to height gradient at north and south boundaries to make them geostrophically balanced. 

Run with matlab or octave:
shallow_water_model
then
animate
